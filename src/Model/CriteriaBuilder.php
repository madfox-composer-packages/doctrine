<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model;


class CriteriaBuilder
{

	public static function createNonDeletedCriteria(string $alias = ''): \Doctrine\Common\Collections\Criteria
	{
		return \Doctrine\Common\Collections\Criteria::create()
			->andWhere(\Doctrine\Common\Collections\Criteria::expr()->eq($alias ? "$alias.isDeleted" : 'isDeleted', FALSE))
			;
	}

	public static function createSortByDefaultCriteria(string $alias = ''): \Doctrine\Common\Collections\Criteria
	{
		$key = $alias ? "$alias.isDefault" : 'isDefault';

		return \Doctrine\Common\Collections\Criteria::create()
			->orderBy([
				$key => 'DESC',
			]);
	}


	public static function createDefaultImageCriteria(bool $default = TRUE, string $alias = ''): \Doctrine\Common\Collections\Criteria
	{
		return \Doctrine\Common\Collections\Criteria::create()
			->andWhere(\Doctrine\Common\Collections\Criteria::expr()->eq($alias ? "$alias.isDefault" : 'isDefault', $default))
			;
	}

}
