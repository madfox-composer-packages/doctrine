<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Locale\Contract;

interface LocaleProviderFactoryInterface
{

	public function create(): \Knp\DoctrineBehaviors\Contract\Provider\LocaleProviderInterface;

}
