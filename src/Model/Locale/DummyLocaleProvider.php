<?php declare(strict_types=1);

namespace Mdfx\Doctrine\Model\Locale;

class DummyLocaleProvider implements \Knp\DoctrineBehaviors\Contract\Provider\LocaleProviderInterface
{
	public function provideCurrentLocale(): ?string
	{
		return 'cs';
	}

	public function provideFallbackLocale(): ?string
	{
		return 'cs';
	}
}
