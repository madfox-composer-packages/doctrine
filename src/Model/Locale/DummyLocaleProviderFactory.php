<?php declare(strict_types=1);

namespace Mdfx\Doctrine\Model\Locale;

use JetBrains\PhpStorm\Pure;

class DummyLocaleProviderFactory implements \Mdfx\Doctrine\Model\Locale\Contract\LocaleProviderFactoryInterface
{

	#[Pure] public function create(): \Knp\DoctrineBehaviors\Contract\Provider\LocaleProviderInterface
	{
		return new DummyLocaleProvider();
	}

}
