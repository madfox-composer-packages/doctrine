<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Repository;

abstract class BaseRepository
{

	/**
	 * @var \Doctrine\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
	 */
	protected $repository;

	protected static ?string $entityClassName = NULL;
	protected \Doctrine\ORM\EntityManager $em;


	public function __construct(
		\Mdfx\Doctrine\Model\EntityManager $em
	)
	{
		$this->em = $em;
		$this->setRepository();
	}


	private function setRepository(): void
	{
		if (static::$entityClassName === NULL) {
			throw new \UnexpectedValueException('Repository name has not been set');
		}

		$this->repository = $this->em->getRepository(static::$entityClassName);
	}

	/**
	 * @return \Doctrine\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
	 */
	public function getRepository()
	{
		return $this->repository;
	}

	/**
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function flush(): void
	{
		$this->em->flush();
	}

	/**
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function persist(\Mdfx\Doctrine\Model\Entity\BaseEntity $object): void
	{
		$this->em->persist($object);
	}

	public function merge(\Mdfx\Doctrine\Model\Entity\BaseEntity $entity): void
	{
		$this->em->merge($entity);
	}

	public function disableSoftDeletable(): void
	{
		$this->em->getFilters()->disable(\Mdfx\Doctrine\Model\Filters::SOFT_DELETABLE->value);
	}

	public function enableSoftDeletable(): void
	{
		$this->em->getFilters()->enable(\Mdfx\Doctrine\Model\Filters::SOFT_DELETABLE->value);
	}

	public function findDeleted(int $id): ?\Mdfx\Doctrine\Model\Entity\BaseEntity
	{
		$this->disableSoftDeletable();

		$entity = $this->find($id);

		$this->enableSoftDeletable();

		return $entity;
	}

	public function remove(\Mdfx\Doctrine\Model\Entity\BaseEntity $entity): void
	{
		$this->em->remove($entity);
		$this->em->flush();
	}

	public function undoRemove(\Mdfx\Doctrine\Model\Entity\BaseEntity $entity): void
	{
		$entity->setDeletedAt(NULL);
		$this->em->persist($entity);
		$this->em->flush();
	}

	public function find(int $id): ?\Mdfx\Doctrine\Model\Entity\BaseEntity
	{
		return $this->getRepository()->find($id);
	}

	public function getEntityName(): string
	{
		if ( ! static::$entityClassName) {
			throw new \Mdfx\Doctrine\Exception\Model\EntityClassNameNotSetException();
		}

		return static::$entityClassName;
	}

	public function findAll(): array
	{
		return $this->getRepository()->findAll();
	}

}
