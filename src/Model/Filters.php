<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model;

enum Filters: string
{

	case SOFT_DELETABLE = 'softdeleteable';

}
