<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model;

class EntityManager extends \Doctrine\ORM\EntityManager
{

	public static function create(
		$connection,
		\Doctrine\ORM\Configuration $config,
		\Doctrine\Common\EventManager $eventManager = NULL
	): EntityManager {
		$connection = static::createConnection($connection, $config, $eventManager);

		return new EntityManager($connection, $config, $connection->getEventManager());
	}

}
