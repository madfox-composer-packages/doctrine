<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Schema;


class BaseEntitySchema
{

	public static function build(\Doctrine\ORM\Mapping\ClassMetadata $classMetadata): \Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder
	{
		$builder = new \Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder($classMetadata);

		$builder->createField('id', 'integer')
			->makePrimaryKey()
			->generatedValue()
			->build();

		$builder->createField('createdAt', 'datetime')
			->option('default', 'CURRENT_TIMESTAMP')
			->build();

		$builder->createField('updatedAt', 'datetime')
			->columnDefinition('DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP')
			->build();

		$builder->createField('deletedAt', 'datetime')
			->nullable()
			->build();

		$builder->createField('isDeleted', 'boolean')
			->option('default', 0)
			->build();

		return $builder;
	}

}
