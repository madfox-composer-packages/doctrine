<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Entity;


interface EntityWithImages
{

	public function getImages(): \Doctrine\Common\Collections\Collection;

}
