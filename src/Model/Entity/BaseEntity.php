<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Entity;

#[\Doctrine\ORM\Mapping\MappedSuperclass]
#[\Gedmo\Mapping\Annotation\SoftDeleteable(fieldName: 'deletedAt', timeAware: true, hardDelete: true)]
abstract class BaseEntity
{

	use \Gedmo\Timestampable\Traits\TimestampableEntity;
	use \Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

	#[\Doctrine\ORM\Mapping\Id]
	#[\Doctrine\ORM\Mapping\GeneratedValue]
	#[\Doctrine\ORM\Mapping\Column]
	private ?int $id = null;

	public function getId(): ?int
	{
		return $this->id;
	}

}
