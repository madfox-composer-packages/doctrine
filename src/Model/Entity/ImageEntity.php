<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Model\Entity;


interface ImageEntity
{

	public function getMediaKey(): string;

	public function getUrl(): string;

	public function isDefault(): bool;

}
