<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Exception\Model;


class EntityClassNameNotSetException extends \RuntimeException
{

}
