<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\DI;

class DoctrineExtension extends \Nette\DI\CompilerExtension
{

	public function loadConfiguration()
	{
		$config = (array) $this->getConfig();

		$builder = $this->getContainerBuilder();

		$metaDataConfiguration = $builder->addDefinition($this->prefix('attributeMetaDataConfiguration'))
			->setFactory('\Doctrine\ORM\Tools\Setup::createAttributeMetadataConfiguration', [
				$config['entityPaths'],
				$config['debug'],
				$config['proxyDir'],
			])
			->addSetup('setNamingStrategy', [
				new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy(),
			])
		;

		if ($config['debug']) {
			$metaDataConfiguration->addSetup('setAutoGenerateProxyClasses', [
				\Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_ALWAYS,
			]);
		} else {
			$metaDataConfiguration->addSetup('setAutoGenerateProxyClasses', [
				\Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_NEVER,
			]);
		}

		$builder->addDefinition($this->prefix('entityManager'))
			->setFactory('\Mdfx\Doctrine\DI\EntityManagerFactory::create', [
				$config,
				$metaDataConfiguration,
			])
		;
	}

	public function getConfigSchema(): \Nette\Schema\Schema
	{
		return \Nette\Schema\Expect::structure([
			'dbname' => \Nette\Schema\Expect::string()->required(),
			'user' => \Nette\Schema\Expect::string()->required(),
			'password' => \Nette\Schema\Expect::string()->required(),
			'host' => \Nette\Schema\Expect::string()->required(),
			'driver' => \Nette\Schema\Expect::string('pdo_mysql'),
			'cacheDir' => \Nette\Schema\Expect::string(),
			'proxyDir' => \Nette\Schema\Expect::string(),
			'entityPaths' => \Nette\Schema\Expect::array(),
			'debug' => \Nette\Schema\Expect::bool(FALSE),
			'charset' => \Nette\Schema\Expect::string('utf8'),
			'collate' => \Nette\Schema\Expect::string('utf8_general_ci'),
		]);
	}

}
