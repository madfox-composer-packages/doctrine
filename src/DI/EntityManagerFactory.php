<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\DI;

class EntityManagerFactory
{

	public static function create(
		array $config,
		\Doctrine\ORM\Configuration $configuration
	): \Mdfx\Doctrine\Model\EntityManager
	{
		self::setupCache($config, $configuration);

		$configuration->addFilter(
			\Mdfx\Doctrine\Model\Filters::SOFT_DELETABLE->value,
			\Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter::class,
		);

		$eventManager = self::createEventManager();

		$entityManager = \Mdfx\Doctrine\Model\EntityManager::create($config, $configuration, $eventManager);
		$entityManager->getFilters()->enable(\Mdfx\Doctrine\Model\Filters::SOFT_DELETABLE->value);

		self::setupCustomFunctions($entityManager);

		return $entityManager;
	}

	private static function createEventManager(): \Doctrine\Common\EventManager
	{
		$eventManager = new \Doctrine\Common\EventManager();

		$timestampableListener = new \Gedmo\Timestampable\TimestampableListener();
		$softDeleteableListener = new \Gedmo\SoftDeleteable\SoftDeleteableListener();
		$translatableListener = new \Knp\DoctrineBehaviors\EventSubscriber\TranslatableEventSubscriber(
			new \Mdfx\Doctrine\Model\Locale\DummyLocaleProvider(),
			'EAGER',
			'EAGER',
		);

		$eventManager->addEventSubscriber($timestampableListener);
		$eventManager->addEventSubscriber($softDeleteableListener);
		$eventManager->addEventSubscriber($translatableListener);

		return $eventManager;
	}

	private static function setupCustomFunctions(\Doctrine\ORM\EntityManager $entityManager): void
	{
		$entityManager->getConfiguration()->addCustomNumericFunction(
			'rand',
			\DoctrineExtensions\Query\Mysql\Rand::class,
		);

		$entityManager->getConfiguration()->addCustomStringFunction(
			'match',
			\DoctrineExtensions\Query\Mysql\MatchAgainst::class,
		);

		$entityManager->getConfiguration()->addCustomStringFunction(
			'field',
			\DoctrineExtensions\Query\Mysql\Field::class,
		);
	}

	private static function setupCache(
		array $config,
		\Doctrine\ORM\Configuration $configuration,
	): void
	{
		if ($config['debug']) {
			return;
		}

		$queryCache = new \Symfony\Component\Cache\Adapter\PhpFilesAdapter('doctrine_queries', 0, $config['cacheDir']);
		$configuration->setQueryCache($queryCache);

		$resultCache = new \Symfony\Component\Cache\Adapter\PhpFilesAdapter('doctrine_results', 0, $config['cacheDir']);
		$configuration->setResultCache($resultCache);
	}

}
