<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\Util;


class ArrayUtil
{
	/**
	 * @param iterable|\Mdfx\Doctrine\Model\Entity\BaseEntity[] $collection
	 */
	public static function getCollectionIds(iterable $collection): array
	{
		$ids = [];

		foreach ($collection as $entity) {
			$ids[] = $entity->getId();
		}

		return $ids;
	}

	/**
	 * @param iterable|\Mdfx\Doctrine\Model\Entity\BaseEntity[] $array
	 */
	public static function indexDoctrineArray(iterable $array): array
	{
		$temp = [];

		foreach ($array as $element) {
			$temp[$element->getId()] = $element;
		}

		return $temp;
	}


	/**
	 * @param array|\Doctrine\Common\Collections\Collection $entities
	 */
	public static function assocMultilang($entities, string $locale, string $key = 'name'): array
	{
		$data = [];

		foreach ($entities as $entity) {
			$data[$entity->getId()] = $entity->translate($locale)->getName();
		}

		return $data;
	}

}
